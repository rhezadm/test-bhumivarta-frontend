import React from 'react';
import {NavLink} from "react-router-dom";
import MapsData from '../../../../data/maps_data.json';

class SideBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isToggled: props.isToggled
        }
    }

    toggleSideBar = () => {
        this.setState({
            isToggled: !this.state.isToggled
        })
    }
    

    render() {
        return (
            <nav className={`navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0 sidenav toggled `}>
                <div className="container-fluid d-flex flex-column p-0">
                    <NavLink className="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0"
                       activeClassName="active"
                       to="/dashboard">
                        <div className="sidebar-brand-icon rotate-n-15"><i className="fas fa-braille"></i></div>
                        <div className="sidebar-brand-text mx-3"><span>Admin</span></div>
                    </NavLink>
                    <hr className="sidebar-divider my-0"/>
                    <div className="sidebar-heading text-left mt-1"><span>Nama Kota</span></div>
                    <ul className="nav navbar-nav text-light sidenav-menu" id="accordionSidebar">
                    {MapsData.map((DataDetail,index)=>{
                        var Loc = DataDetail.Name.replace(" ", "_");
                        return <li className="nav-item" role="presentation">
                        <NavLink className="nav-link" activeClassName="active" to={`/dashboard?Loc=`+Loc+`&La=`+DataDetail.Latitude+`&Long=`+DataDetail.Longitude}>
                            <span>{DataDetail.Name}</span>
                        </NavLink>
                    </li>
                    })}                                                
                    </ul>
                </div>
            </nav>
        );
    }
}

export default SideBar;