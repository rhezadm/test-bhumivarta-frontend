import React, {useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom";
import DarkMode from "../DarkMode";



const TopBar = props => {
    return (
        <nav className={`navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top`}>
            <div className="container-fluid">
                <button className="btn rounded-circle mr-3" id="sidebarToggleTop" type="button">
                    <i className="fas fa-bars"/>
                </button>
                <ul className="nav navbar-nav flex-nowrap ml-auto">
                    <li className="nav-item dropdown no-arrow mx-1">
                        <a className="nav-link">
                            <DarkMode />
                        </a>
                    </li>
                    <div className="d-none d-sm-block topbar-divider"/>
                    <li className="nav-item dropdown no-arrow mx-1">
                        <a className="nav-link">
                            Bahasa
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default TopBar;