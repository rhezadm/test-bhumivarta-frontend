import React from 'react';
import {Switch, Redirect} from 'react-router-dom';
import {RouteWithLayout} from './components';
import {Layout} from './layouts'
import {Dashboard} from './pages'

const Routes = () => {
    return (
        <Switch>
            <Redirect
                exact
                from="/"
                to="/dashboard"
            />
            <RouteWithLayout
                component={Dashboard}
                exact
                layout={Layout}
                path="/dashboard"
            />
            <Redirect to="/not-found"/>
        </Switch>
    );
};

export default Routes;
  