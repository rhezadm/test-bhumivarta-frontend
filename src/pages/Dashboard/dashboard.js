import React from 'react';
import {Maps} from '../../layouts/Layout/components';
import { Map, Marker } from "pigeon-maps"
import { stamenToner } from 'pigeon-maps/providers'

const search = window.location.search;
const params = new URLSearchParams(search);
const x = params.get('Long');
const y = params.get('La');
const Dashboard = props => {
    return (
        <div className="container-fluid">
            <div className="d-sm-flex justify-content-between align-items-center mb-4">
                <div id="maps">
                <Map  height={400} width={1000} defaultCenter={[-6.200000 , 106.816666]} defaultZoom={11}>
                    <Marker width={50} anchor={[-6.200000 , 106.816666]} />
                </Map>
                </div>
            </div>
        </div>
        
    );
};

export default Dashboard;